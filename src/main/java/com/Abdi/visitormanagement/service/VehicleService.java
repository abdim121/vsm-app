package com.Abdi.visitormanagement.service;

import com.Abdi.visitormanagement.domain.Vehicle;


import java.util.List;

public interface VehicleService {
    void save(Vehicle vehicle);

    List<Vehicle> getVehicleListByGroupId(String groupId);

    void updateVehicleNumber(int id, String vehicleNumber);
}
