package com.Abdi.visitormanagement.service;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import java.io.IOException;

public interface MailService {
    void sendmail(String name, String toMail, String fromMail) throws AddressException, MessagingException, IOException;
}
