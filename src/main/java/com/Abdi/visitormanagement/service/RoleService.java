package com.Abdi.visitormanagement.service;

public interface RoleService {
    String findByRoleId(int id);
    void updateRoleByRoleId(String role, int id);
}