package com.Abdi.visitormanagement.service;

import com.Abdi.visitormanagement.domain.Request;
import com.Abdi.visitormanagement.domain.User;

import java.util.List;
import java.util.Map;

public interface UserService {

    User getUsersByUsername(String username);

    void saveUser(Map<String, String> user);

    List<User> getAllUsers();

    void deleteUser(String userName);

    User getAdmin(String hodEmail);

    List<Request> getRequest();

    List<Request> getRequestByUserName(int employee_id);

    int getIdByUserName(String userName);

    void updatePassword(String userName, String password);

    void updateUser(String userName, String email,String hodMail,String department);
}