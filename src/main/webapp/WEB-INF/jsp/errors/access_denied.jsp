<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org">
<head>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery-3.3.1.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/javascript/javas.js"/>"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="<c:url value="/resources/css/navVisitor.css"/>">
</head>
<style>

    .acessDen {
        width: 500px;
        margin-top: 5%;
        margin-left: 35%;
    }

</style>
<body>
<%@ include file="../userHome/header.jsp" %>
<div class="container">
    <img class="acessDen" src="<c:url value="/resources/images/accessdenied.png"/> ">
</div>
</body>
</html>