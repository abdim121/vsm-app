<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<head>
    <title>Search</title>
    <script type="text/javascript" src="<c:url value="/resources/javascript/javas.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery-3.3.1.min.js"/>"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<c:url value="/resources/css/navVisitor.css"/>">
    <style>
        .list-group-item {
            cursor: pointer;
        }
    </style>
</head>
<body>
<%@ include file="../userHome/header.jsp" %>
<div class="container">
    <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Visitor details</h3>
    <div class="card-body">
        <div class="active-cyan-3 active-cyan-4 mb-4">
            <input class="form-control" id="myInput" type="text" placeholder="Search by DL/ID" aria-label="Search"
                   onkeyup="search()">
        </div>
        <div id="table" class="table-editable">
            <table id="userInfo" class="table table-bordered table-responsive-md table-striped text-center">
                <thead>
                <th>DL/ID Number</th>
                <th>Name</th>
                <th>Company</th>
                <th>Date</th>
                <th>Vehicle Plate Number</th>
                <th>Pass ID</th>
                <th>Check In</th>
                </thead>
                <tbody id="visitorList">
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
     id="deleteUserModal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="card-body">
                        <h5 class="modal-title" id="myModalLabel">Do you want to delete the user </h5>
                        <b><p id="request"></p></b>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="modal-btn-si" onclick="deleteYes()">Yes</button>
                <button type="button" class="btn btn-primary" id="modal-btn-no" onclick="deleteNo()">No</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="vehicleDetailsModal" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 1200px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Vehicle Details</h3>
                    <div class="card-body">
                        <div id="tables" class="table-editable">
                            <table class="table table-bordered table-responsive-md table-striped text-center">
                                <thead>
                                <th>Vehicle Plate Number</th>
                                <th>Update Number</th>
                                </thead>
                                <tbody id="vehicleTable">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<%--No Vehicles Modal--%>
<div class="modal fade" id="noVehiclesModal" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 1200px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Vehicle Details</h3>
                    <div class="card-body">
                        <div class="table-editable">
                            <p class="text-center">No Vehicles!</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<%--Modal PassId Checkin--%>
<div class="modal fade" id="passIdCheckInModal" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 1200px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Pass Id and Check In
                        Time</h3>
                    <div class="card-body">
                        <div class="table-editable">
                            <table class="table table-bordered table-responsive-md table-striped text-center">
                                <thead>
                                <th>Pass Id</th>
                                <th>Check In</th>
                                </thead>
                                <tbody id="passIdCheckIn">
                                <td><input type="text" class="text-center col-sm-5" id="passIdInput"></td>
                                <td><input type="time" class="text-center col-sm-8" id="checkInInput" value="now"></td>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="updatePassIdCheckIn" class="btn btn-outline-success">
                    Save
                </button>
            </div>
        </div>
    </div>
</div>
<%--CehckoutModal--%>
<div class="modal fade" id="checkoutModal" role="dialog">
    <div class="modal-dialog modal-lg" style="width: 1200px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Check Out Time</h3>
                    <div class="card-body">
                        <div class="table-editable">
                            <table class="table table-bordered table-responsive-md table-striped text-center">
                                <thead>
                                <th>Check Out</th>
                                </thead>
                                <tbody>
                                <td><input  type="time" class="text-center col-sm-3" id="checkOutInput" value="now"></td>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="updateCheckOut" class="btn btn-outline-success" >Save
                </button>
            </div>
        </div>
    </div>
</div>
<%--ErrorModal--%>
<div class="modal fade" id="errorInPassIdOrCheckIn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <i class="fa fa-times-circle" style="color:red;font-size: 30px"></i>  Please fill Correctly
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<%--Sure Modal PassId--%>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
     id="sureModalPassIdCheckIn">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="card-body">
                        <h5 class="text-center modal-title">Are you sure</h5>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="yseButton" >Yes</button>
                <button type="button" class="btn btn-primary" id="noButton" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<%--Sure Modal CheckOut--%>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
     id="sureModalCheckOut">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="card-body">
                        <h5 class="text-center modal-title">Are you sure</h5>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="yseButtonCheckout" >Yes</button>
                <button type="button" class="btn btn-primary"  data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<%--SuccessModal--%>
<div class="modal fade" id="successModalForUpdatingVehicleNumber" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <i class="text-center fa fa-check-circle" style="color:green;font-size: 30px"></i>  SuccessFully Updated
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    $(document).ready(function () {
        getVisitorDetailsForCheckInAndCheckOut();
        $("#myInput").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#visitorList tr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>
</html>