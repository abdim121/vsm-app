<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery-3.3.1.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/javascript/javas.js"/>"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<c:url value="/resources/css/navVisitor.css"/>">
    <style>


        .container
        {
            margin-top:10%;
        }
        a.homeTag
        {
            text-decoration: none;
            color: black;
        }
        a.homeTag:hover
        {
            color:white;
        }
        div.col-sm-4:hover
        {
            background-color: darkgrey;
        }
        #homebody
        {
            background-color: #D3D3D3;
        }
        .nav-link{
            cursor: pointer;
        }

    </style>
</head>
<body id="homebody">
<%@ include file="header.jsp" %>
<h4 style="margin-left:5%"> Welcome <span id="welcomeUserName"></span> !</h4>
<div class="container">
   <div class="starter-template">

   <c:if test="${role == 1}">
   <h1>Your Requests</h1>
         <table
          class="table table-striped table-hover table-condensed table-bordered">
          <tr>
           <th>#</th>
           <th>Name</th>
           <th>Purpose</th>
           <th>Company</th>

          </tr>
          <c:forEach var="request" items="${requests}" varStatus="i">
           <tr>
           <td>${i.index + 1}</td>
            <td>${request.name}</td>
            <td>${request.purpose}</td>
            <td>${request.company}</td>

           </tr>
          </c:forEach>
         </table>
   </c:if>
<c:if test="${role == 2}">
<h1>All Requests</h1>
         <table
          class="table table-striped table-hover table-condensed table-bordered">
          <tr>
           <th>#</th>
           <th>Name</th>
           <th>Purpose</th>
           <th>Company</th>
          </tr>
          <c:forEach var="requestAdmin" items="${requestsAdmin}" varStatus="i">
           <tr>
           <td>${i.index + 1}</td>
            <td>${requestAdmin.name}</td>
            <td>${requestAdmin.purpose}</td>
            <td>${requestAdmin.company}</td>
           </tr>
          </c:forEach>
         </table>
      </c:if>
     </div>
</div>

<%--Success Modal--%>
<div class="modal fade" id="successModalForChangePassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <i class="fa fa-check-circle" style="color:green;font-size: 30px"></i> Successfully Changed Password
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<%--Error Modal--%>
<div class="modal fade" id="errorModalForChangePassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <i class="fa fa-times-circle" style="color:red;font-size: 30px"></i> Password do not match
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<%--Error Modal--%>
<div class="modal fade" id="errorModalForCharacterChangePassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <i class="fa fa-times-circle" style="color:red;font-size: 30px"></i> Password should contain at least 6 characters
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    $(document).ready(function () {
        getUserRoll();
        getAllRequest();
        getAllRequestForUser();
        getUserName();

    });
</script>

</html>
