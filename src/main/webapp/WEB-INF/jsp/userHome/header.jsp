<head>
<script type="text/javascript" src="<c:url value="/resources/javascript/javas.js"/>"></script>
</head>
<nav class="navbar navbar-expand-lg ">
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="<c:url value="/"/>">Home <span><i class="fa fa-home"></i></span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<c:out  value="/visitorRequest"/>">Requests
                    <span><i class="fa fa-user-plus"></i></span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<c:out value="/getRequests"/>">Approval<span> <i
                        class="fa fa-legal"></i> &nbsp;<span class="badge badge-light" id="newRequestForAdmin"></span></span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link"
                   href="<c:out value="/userRequests"/>">Status<span> <i
                        class="fa fa-bell"></i>&nbsp;<span class="badge badge-light" id="newRequestForUser"></span></span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<c:out  value="/search"/>">Search
                    <span> <i class="fa fa-clock-o"></i></span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Settings <span><i class="fa fa-cog"></i></span>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a style="color:black" class="dropdown-item"
                       href="<c:out value="/registration"/>"><i
                            class="fa fa-edit"></i> Create Account</a>
                    <a style="color:black" class="dropdown-item"
                       href="<c:out value="/allUsers"/>"><i
                            class="fa fa-id-badge"></i> Accounts</a>
                    <a style="color:black"  class="dropdown-item" href="<c:out value="/Reports"/>"><i class="fa fa-tasks"></i> Reports</a>
                 <a style="color:black"  class="dropdown-item"  onclick="showChangePasswordModal()"><i class="fa fa-key"></i>Change Password<span> </span></a>
                </div>
            </li>
        </ul>
    </div>
    <div class="navbar-collapse collapse w-1 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">

            <li class="nav-item">
                <a class="nav-link" href="<c:out value="/logout"/>">Sign Out<span> <i
                        class="fa fa-power-off"></i></span></a>
            </li>

        </ul>
    </div>

</nav>
<br>
<div class="modal fade" id="modalChangePassword" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content -lg">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Change Password</h3>
                    <p>New Password :</p>
                    <input type="password" class="form-control" id="changePassword">
                    <p>Confirm Password :</p>
                    <input type="password" class="form-control" id="confirmChangePassword">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a class="btn btn-outline-success" onclick="changePassword()">Save</a>
            </div>
        </div>
    </div>
</div>