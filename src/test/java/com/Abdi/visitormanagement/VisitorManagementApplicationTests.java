package com.Abdi.visitormanagement;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootTest
class VisitorManagementApplicationTests {

	public static void main(String[] args) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String rawPass = "power";
		String encoded = encoder.encode(rawPass);
		System.out.println(encoded);

	}
}
